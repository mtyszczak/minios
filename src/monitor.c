#include "monitor.h"

enum color defFG = COLOR_WHITE;
enum color defBG = COLOR_BLACK;

u16int *const video_memory = (u16int*) 0xB8000; // video location


u8int lastX = 0, lastY = 0; // Last used x and y coords for printing new lines

enum color getDefFG() {
	return defFG;
}

enum color getDefBG() {
	return defBG;
}

void putc(u8int x, u8int y, enum color fg, enum color bg, char c) {
	video_memory[y * COLS + x] = (bg << 12) | (fg << 8) | c; // put char at specified x and y coords with given fg and bg color
	update_cursor();
}

void clear(enum color bg) {
    u8int x, y;
    lastX = 0; lastY = 0;
    for (y = 0; y < ROWS; y++)
        for (x = 0; x < COLS; x++)
            putc(x, y, bg, bg, ' '); // clear the screen with given bg color
}

void puts(u8int x, u8int y, enum color fg, enum color bg, const char *s) {
    lastX = x + strlen(s);
    for (; *s; s++, x++) {
			// TODO: if (x>COLS) \n
// TODO: vvv \n \f \t implementation (not working for now)
/*        if (s[x] == '\n') {
            y+=1;
            x=0;
            lastY+=1;
            lastX=0;
		        } else if (s[x] == '\f') {
            clear(defBG);
            x=0;
            y=0;
            lastX=0;
            lastY=0;
        } else if (s[x] == '\t') {
            x+=8;
            lastX+=7;
        } else { */
        putc(x, y, fg, bg, *s); // put string at specified x and y coords with given fg and bg color
        // }
				// check_scroll(); // TODO: not working for now :/
    }
}

void print(char *s) {
    puts(lastX, lastY, defFG, defBG, s);
}

void println(char *s) {
    print(s);
    lastY+=1;
    lastX=0;
}

void setForegroundColor(enum color fg) {
    defFG = fg;
}

void setBackgroundColor(enum color bg) {
    defBG = bg;
}

char get_monitor_char(u8int x, u8int y) {
	char tempC = video_memory[y*COLS+x] - ((defBG << 12)|(defFG << 8));
	return tempC; // TODO: not working when bg or/and fg is different from defBG/defFG
	// TODO: make another video_memory like array that stores ((defFG<<x)|(defBG<<y)) to solve that ^^^ problem
}

// Scrolls the text on the screen up by one line.
void check_scroll() {
   // Get a space character with the default colour attributes.
   u16int blank = (defBG << 12)|(defFG << 8) | 0x20 /* space */;

   // Row 25 is the end, this means we need to scroll up
   if(lastY >= ROWS)
   {
       // Move the current text chunk that makes up the screen
       // back in the buffer by a line
       int i;
       for (i = 0*COLS; i < (ROWS-1)*COLS; i++)
       {
           video_memory[i] = video_memory[i+COLS];
       }

       // The last line should now be blank. Do this by writing
       // 80 spaces to it.
       for (i = (ROWS-1)*COLS; i <ROWS*COLS; i++)
       {
           video_memory[i] = blank;
       }
       // The cursor should now be on the last line.
       lastY = ROWS-1;
   }
}

// Update the hardware cursor to given x and y coordinates
void move_cursor(u8int x, u8int y) {
	if (get_monitor_char(x,y)==32) // ' '
		video_memory[y * COLS + x] =  (defBG << 12) | (defFG << 8) | ' ';

   u16int cursorLocation = y * COLS + x;

	 outb(0x3D4, 0x0F);                  // Tell the VGA board we are setting the low cursor byte.
   outb(0x3D5, cursorLocation&0xFF);      // Send the low cursor byte.

   outb(0x3D4, 0x0E);                  // Tell the VGA board we are setting the high cursor byte.
   outb(0x3D5, (cursorLocation >> 8)&0xFF); // Send the high cursor byte.
}

void update_cursor() {
	move_cursor(lastX, lastY);
}
