#include "keyboard.h"

int default_keyboard_code = KEYBOARD_CODE_EN;

void set_default_keyboard_code(int keyboard_code) {
	default_keyboard_code = keyboard_code;
}

// TODO: Custom font
wchar_t altchar(char keychar) { // return keychar if there is no "alt char"
	switch (default_keyboard_code) {
		case KEYBOARD_CODE_EN: // English
			switch(keychar) {
				default:
					return keychar;
					break;
			}
			break;
		case KEYBOARD_CODE_PL: // Polish
			switch(keychar) {
                                case 'e':
                                        return L'ę'; // ę
                                        break;
                                case 'o':
                                        return L'ó'; // ó
                                        break;
                                case 'a':
                                        return L'ą'; // ą
                                        break;
                                case 's':
                                        return L'ś'; // ś
                                        break;
                                case 'l':
                                        return L'ł'; // ł
                                        break;
                                case 'z':
                                        return L'ż'; // ż
                                        break;
                                case 'x':
                                        return L'ź'; // ź
                                        break;
                                case 'c':
                                        return L'ć'; // ć
                                        break;
                                case 'n':
                                        return L'ń'; // ń
                                        break;
                                case 'E':
                                        return L'Ę'; // Ę
                                        break;
                                case 'O':
                                        return L'Ó'; // Ó
                                        break;
                                case 'A':
                                        return L'Ą'; // Ą
                                        break;
                                case 'S':
                                        return L'Ś'; // Ś
                                        break;
                                case 'L':
                                        return L'Ł'; // Ł
                                        break;
                                case 'Z':
                                        return L'Ż'; // Ż
                                        break;
                                case 'X':
                                        return L'Ź'; // Ź
                                        break;
                                case 'C':
                                        return L'Ć'; // Ć
                                        break;
                                case 'N':
                                        return L'Ń'; // Ń
                                        break;
				default:
					return keychar;
					break;
			}
			break;
		case KEYBOARD_CODE_CS: // Czech
			switch(keychar) {
				// TODO: https://pl.wikipedia.org/wiki/Klawiatura_programisty
				default:
					return keychar;
					break;
			}
			break;
		case KEYBOARD_CODE_RO: // Romanian
			switch(keychar) {
				// TODO: https://pl.wikipedia.org/wiki/Klawiatura_programisty
				default:
					return keychar;
					break;
			}
			break;
		default:
			return keychar;
			break;
}
}


char shiftchar(char keychar) { // return keychar if there is no "shift char"
	switch(keychar) {
		case '`':
			return '~';
			break;
		case '1':
			return '!';
			break;
		case '2':
			return '@';
			break;
        case '3':
            return '#';
            break;
        case '4':
			return '$';
            break;
        case '5':
            return '%';
            break;
        case '6':
            return '^';
            break;
        case '7':
            return '&';
            break;
		case '8':
			return '*';
			break;
		case '9':
			return '(';
			break;
		case '0':
			return ')';
			break;
		case '-':
			return '_';
			break;
		case '=':
			return '+';
			break;
		case ']':
			return '}';
			break;
		case '[':
			return '{';
			break;
		case '\\':
			return '|';
			break;
		case '\'':
			return '\"';
			break;
		case ';':
			return ':';
			break;
		case '/':
			return '?';
			break;
		case '.':
			return '>';
			break;
		case ',':
			return '<';
			break;
		case 'q':
			return 'Q';
			break;
		case 'w':
			return 'W';
			break;
		case 'e':
			return 'E';
			break;
		case 'r':
			return 'R';
			break;
		case 't':
			return 'T';
			break;
		case 'y':
			return 'Y';
			break;
		case 'u':
			return 'U';
			break;
		case 'i':
			return 'I';
			break;
		case 'o':
			return 'O';
			break;
		case 'p':
			return 'P';
			break;
		case 'a':
			return 'A';
			break;
		case 's':
			return 'S';
			break;
		case 'd':
			return 'D';
			break;
		case 'f':
			return 'F';
			break;
		case 'g':
			return 'G';
			break;
		case 'h':
			return 'H';
			break;
		case 'j':
			return 'J';
			break;
		case 'k':
			return 'K';
			break;
		case 'l':
			return 'L';
			break;
		case 'z':
			return 'Z';
			break;
		case 'x':
			return 'X';
			break;
		case 'c':
			return 'C';
			break;
		case 'v':
			return 'V';
			break;
		case 'b':
			return 'B';
			break;
		case 'n':
			return 'N';
			break;
		case 'm':
			return 'M';
			break;
		default:
			return 0;
			break;
}
}

char codeToChar(char code) { 
	switch(code) { 
/*		case KEYPAD_DIV: // Same as normal div
			return '/';
			break;
		case KEYPAD_MUL:
			return '*';
			break;
		case KEYPAD_SUB:
			return '-';
			break;
		case KEYPAD_ADD:
			return '+';
			break;
		case KEYPAD_DOT:
			return '.';
			break;
		case KEYPAD_0:
			return '0';
			break;
		case KEYPAD_1:
			return '1';
			break;
		case KEYPAD_2:
			return '2';
			break;
		case KEYPAD_3:
			return '3';
			break;
		case KEYPAD_4:
			return '4';
			break;
		case KEYPAD_5:
			return '5';
			break;
		case KEYPAD_6:
			return '6';
			break;
		case KEYPAD_7:
			return '7';
			break;
		case KEYPAD_8:
			return '8';
			break;
		case KEYPAD_9:
			return '9';
			break;
			*/
		case KEY_A:
			return 'a';
			break;
		case KEY_B:
			return 'b';
			break;
		case KEY_C:
			return 'c';
			break;
		case KEY_D:
			return 'd';
			break;
		case KEY_E:
			return 'e';
			break;
		case KEY_F:
			return 'f';
			break;
		case KEY_G:
			return 'g';
			break;
		case KEY_H:
			return 'h';
			break;
		case KEY_I:
			return 'i';
			break;
		case KEY_J:
			return 'j';
			break;
		case KEY_K:
			return 'k';
			break;
		case KEY_L:
			return 'l';
			break;
		case KEY_M:
			return 'm';
			break;
		case KEY_N:
			return 'n';
			break;
		case KEY_O:
			return 'o';
			break;
		case KEY_P:
			return 'p';
			break;
		case KEY_Q:
			return 'q';
			break;
		case KEY_R:
			return 'r';
			break;
		case KEY_S:
			return 's';
			break;
		case KEY_T:
			return 't';
			break;
		case KEY_U:
			return 'u';
			break;
		case KEY_V:
			return 'v';
			break;
		case KEY_W:
			return 'w';
			break;
		case KEY_X:
			return 'x';
			break;
		case KEY_Y:
			return 'y';
			break;
		case KEY_Z:
			return 'z';
			break;
		case KEY_1:
			return '1';
			break;
		case KEY_2:
			return '2';
			break;
		case KEY_3:
			return '3';
			break;
		case KEY_4:
			return '4';
			break;
		case KEY_5:
			return '5';
			break;
		case KEY_6:
			return '6';
			break;
		case KEY_7:
			return '7';
			break;
		case KEY_8:
			return '8';
			break;
		case KEY_9:
			return '9';
			break;
		case KEY_0:
			return '0';
			break;
		case KEY_MINUS:
			return '-';
			break;
		case KEY_EQUAL:
			return '=';
			break;
		case KEY_SQUARE_OPEN_BRACKET:
			return '[';
			break;
		case KEY_SQUARE_CLOSE_BRACKET:
			return ']';
			break;
		case KEY_SEMICOLON:
			return ';'; // 59
			break;
		case KEY_BACKSLASH:
			return '\\';
			break;
		case KEY_COMMA:
			return ',';
			break;
		case KEY_DOT:
			return '.';
			break;
		case KEY_SLASH:
			return '/';
			break;
		case KEY_SINGLE_QUOTE:
			return '\'';
			break;
		case KEY_GRAVE:
			return '`';
			break;
/*
		case KEY_DELETE:
			return 127; // DEL
			break;
		case KEY_ESC:
			return 27; // ESC
			break;
		case KEY_HOME:
			return 13; // CR
			break;
*/

/*		case KEY_ENTER:
			return '\n';
			break; // Not working
		case KEY_BACKSPACE:
			return '\b';
			break;
		case KEY_TAB:
			return '\t';
			break;
*/
		case KEY_SPACE:
			return ' ';
			break;

		default:
			return KEY_UNKNOWN;
			break;
	}
}

char* charToName(char keychar) {
	switch(keychar) {
		case '0':
			return "zero";
			break;
		case '1':
			return "one";
			break;
		case '2':
			return "two";
			break;
		case '3':
			return "three";
			break;
		case '4':
			return "four";
			break;
		case '5':
			return "five";
			break;
		case '6':
			return "six";
			break;
		case '7':
			return "seven";
			break;
		case '8':
			return "eight";
			break;
		case '9':
			return "nine";
			break;
		case 'a':
			return "a";
			break;
		case 'b':
			return "b";
			break;
		case 'c':
			return "c";
			break;
		case 'd':
			return "d";
			break;
		case 'e':
			return "e";
			break;
		case 'f':
			return "f";
			break;
		case 'g':
			return "g";
			break;
		case 'h':
			return "h";
			break;
		case 'i':
			return "i";
			break;
		case 'j':
			return "j";
			break;
		case 'k':
			return "k";
			break;
		case 'l':
			return "l";
			break;
		case 'm':
			return "m";
			break;
		case 'n':
			return "n";
			break;
		case 'o':
			return "o";
			break;
		case 'p':
			return "p";
			break;
		case 'q':
			return "q";
			break;
		case 'r':
			return "r";
			break;
		case 's':
			return "s";
			break;
		case 't':
			return "t";
			break;
		case 'u':
			return "u";
			break;
		case 'v':
			return "v";
			break;
		case 'w':
			return "w";
			break;
		case 'x':
			return "x";
			break;
		case 'y':
			return "y";
			break;
		case 'z':
			return "z";
			break;
		case 'A':
			return "A";
			break;
		case 'B':
			return "B";
			break;
		case 'C':
			return "C";
			break;
		case 'D':
			return "D";
			break;
		case 'E':
			return "E";
			break;
		case 'F':
			return "F";
			break;
		case 'G':
			return "G";
			break;
		case 'H':
			return "H";
			break;
		case 'I':
			return "I";
			break;
		case 'J':
			return "J";
			break;
		case 'K':
			return "K";
			break;
		case 'L':
			return "L";
			break;
		case 'M':
			return "M";
			break;
		case 'N':
			return "N";
			break;
		case 'O':
			return "O";
			break;
		case 'P':
			return "P";
			break;
		case 'Q':
			return "Q";
			break;
		case 'R':
			return "R";
			break;
		case 'S':
			return "S";
			break;
		case 'T':
			return "T";
			break;
		case 'U':
			return "U";
			break;
		case 'V':
			return "V";
			break;
		case 'W':
			return "W";
			break;
		case 'X':
			return "X";
			break;
		case 'Y':
			return "Y";
			break;
		case 'Z':
			return "Z";
			break;
		case ' ':
			return "space";
			break;
		case '\a':
			return "bell";
			break;
		case '\v':
			return "vertical tab";
			break;
		case '\f':
			return "form feed";
			break;
		case '\t':
			return "tab";
			break;
		case '\n':
			return "line feed";
			break;
		case '\r':
			return "carriage return";
			break;
		case '\b':
			return "backspace";
			break;
		case '`':
			return "grave";
			break;
		case '~':
			return "tilde";
			break;
		case '!':
			return "exclamation mark";
			break;
		case '@':
			return "at sign";
			break;
		case '#':
			return "number sign";
			break;
		case '$':
			return "dollar sign";
			break;
		case '%':
			return "percent sign";
			break;
		case '^':
			return "caret";
			break;
		case '&':
			return "ampersand";
			break;
		case '*':
			return "asterisk";
			break;
		case '(':
			return "opening parenthesis";
			break;
		case ')':
			return "closing parenthesis";
			break;
		case '-':
			return "minus sign";
			break;
		case '_':
			return "underscore";
			break;
		case '=':
			return "equal sign";
			break;
		case '+':
			return "plus sign";
			break;
		case '[':
			return "opening bracket";
			break;
		case ']':
			return "closing bracket";
			break;
		case '{':
			return "opening brace";
			break;
		case '}':
			return "closing brace";
			break;
		case '|':
			return "vertical bar";
			break;
		case '\\':
			return "backslash";
			break;
		case '\'':
			return "single quote";
			break;
		case '\"':
			return "double quotes";
			break;
		case ';':
			return "semicolon";
			break;
		case ':':
			return "colon";
			break;
		case '/':
			return "slash";
			break;
		case '?':
			return "question mark";
			break;
		case '.':
			return "period";
			break;
		case ',':
			return "comma";
			break;
		case '>':
			return "greater than sign";
			break;
		case '<':
			return "less than sign";
			break;
		default:
			return "unknown";
			break;
	}
}

char charToCode(char code) {
	switch(code) {
		case '0':
			return KEY_0;
			break;
		case '1':
			return KEY_1;
			break;
		case '2':
			return KEY_2;
			break;
		case '3':
			return KEY_3;
			break;
		case '4':
			return KEY_4;
			break;
		case '5':
			return KEY_5;
			break;
		case '6':
			return KEY_6;
			break;
		case '7':
			return KEY_7;
			break;
		case '8':
			return KEY_8;
			break;
		case '9':
			return KEY_9;
			break;
		case 'a':
			return KEY_A;
			break;
		case 'b':
			return KEY_B;
			break;
		case 'c':
			return KEY_C;
			break;
		case 'd':
			return KEY_D;
			break;
		case 'e':
			return KEY_E;
			break;
		case 'f':
			return KEY_F;
			break;
		case 'g':
			return KEY_G;
			break;
		case 'h':
			return KEY_H;
			break;
		case 'i':
			return KEY_I;
			break;
		case 'j':
			return KEY_J;
			break;
		case 'k':
			return KEY_K;
			break;
		case 'l':
			return KEY_L;
			break;
		case 'm':
			return KEY_M;
			break;
		case 'n':
			return KEY_N;
			break;
		case 'o':
			return KEY_O;
			break;
		case 'p':
			return KEY_P;
			break;
		case 'q':
			return KEY_Q;
			break;
		case 'r':
			return KEY_R;
			break;
		case 's':
			return KEY_S;
			break;
		case 't':
			return KEY_T;
			break;
		case 'u':
			return KEY_U;
			break;
		case 'v':
			return KEY_V;
			break;
		case 'w':
			return KEY_W;
			break;
		case 'x':
			return KEY_X;
			break;
		case 'y':
			return KEY_Y;
			break;
		case 'z':
			return KEY_Z;
			break;
		case ' ':
			return KEY_SPACE;
			break;
		case '\t':
			return KEY_TAB;
			break;
		case '\n':
			return KEY_ENTER;
			break;
		case '\b':
			return KEY_BACKSPACE;
			break;
		case '`':
			return KEY_GRAVE;
			break;
		case '-':
			return KEY_MINUS;
			break;
		case '=':
			return KEY_EQUAL;
			break;
		case '[':
			return KEY_SQUARE_OPEN_BRACKET;
			break;
		case ']':
			return KEY_SQUARE_CLOSE_BRACKET;
			break;
		case '\\':
			return KEY_BACKSLASH;
			break;
		case '\'':
			return KEY_SINGLE_QUOTE;
			break;
		case ';':
			return KEY_SEMICOLON;
			break;
		case '/':
			return KEY_SLASH;
			break;
		case '.':
			return KEY_DOT;
			break;
		case ',':
			return KEY_COMMA;
			break;
		default:
			return KEY_UNKNOWN;
			break;
	}

}
