#ifndef INPUT_H
#define INPUT_H

#include "common.h"
#include <string.h>
#include "monitor.h" // just for tests (remove)
#include "keyboard.h"

char nextchar(bool display);
char *nextline(char *buffer, int buf_size, bool display);

#endif
