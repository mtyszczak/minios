# miniOS
Lightweight operating system concept written in C and Assembly

## Getting Started

### Installing

To install and run our software on Linux use:
```bash
make
make clean # optional
make run-img
```

## Authors

* **[Mateusz Tyszczak](https://gitlab.com/mtyszczak)** - *Developer*
* **[The Nieznany](https://gitlab.com/TheNieznany)** - *Developer*
* **[toslaw](https://github.com/toslaw1)** - *Developer*

## License
See [LICENSE.md](LICENSE.md)
