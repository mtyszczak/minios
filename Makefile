# .POSIX:

# System settings
BITSYS = 32
LDSYS = elf_i386
NASMSYS = elf32

# DIRS settings
# include dir (.h files)
IDIR = include
# source dir (.c and .cpp files)
SDIR = src
# object dir (.o files)
ODIR = obj
# output dir (.img files)
OUTDIR = output
# boot dir (default iso/boot)
BOOTDIR = iso/boot

# COMPILER SETTINGS
CC = gcc
CFLAGS = -I$(IDIR) -c -m$(BITSYS) -std=c99 -ffreestanding
# -fno-builtin -Wall -Wextra -Os

# LINKER SETTINGS
LINKER = linker.ld
LD = ld
LFLAGS = -nostdlib -m $(LDSYS) -T $(LINKER)

# NASM SETTINGS
NASM = nasm
NFLAGS = -f $(NASMSYS)

# RUNNING SETTINGS
VOS = qemu-system-i386
# run
VFLAGSRUN = -kernel
# run-img
VFLAGSISO = -hda

# Grub creator command
GRUBMK = grub-mkrescue
GFLAGS =

# clean (remove)
RM = rm
RFLAGS = -rf
RMFILES = $(ODIR) $(BOOTDIR)/main.elf


# Object filepaths
_OBJ = entry.o main.o common.o input.o keyboard.o monitor.o string.o time.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))


all: init $(_OBJ) main.elf main.img

init:
	mkdir -p $(ODIR) $(OUTDIR)

entry.o: entry.asm
	$(NASM) $(NFLAGS) '$<' -o '$(ODIR)/$@'

main.o: $(SDIR)/main.c
	$(CC) -o '$(ODIR)/$@' '$<' $(CFLAGS)


# Include sources:

# src/common.c -> include/common.h
common.o: $(SDIR)/common.c
	$(CC) -o '$(ODIR)/$@' '$<' $(CFLAGS)

# src/input.c -> include/input.h
input.o: $(SDIR)/input.c
	$(CC) -o '$(ODIR)/$@' '$<' $(CFLAGS)

# src/keyboard.c -> include/keyboard.h
keyboard.o: $(SDIR)/keyboard.c
	$(CC) -o '$(ODIR)/$@' '$<' $(CFLAGS)

# src/monitor.c -> include/monitor.h
monitor.o: $(SDIR)/monitor.c
	$(CC) -o '$(ODIR)/$@' '$<' $(CFLAGS)

# src/string.c -> include/string.h
string.o: $(SDIR)/string.c
	$(CC) -o '$(ODIR)/$@' '$<' $(CFLAGS)

# src/time.c -> include/time.h
time.o: $(SDIR)/time.c
	$(CC) -o '$(ODIR)/$@' '$<' $(CFLAGS)


# main.elf is the the multiboot file.
main.elf: $(OBJ)
	$(LD) $(LFLAGS) -o '$@' $^

main.img: main.elf
	mv '$<' $(BOOTDIR)
	$(GRUBMK) $(GFLAGS) -o '$(OUTDIR)/$@' iso

.PHONY: all clean run run-img

clean:
	$(RM) $(RFLAGS) $(RMFILES)

run: $(BOOTDIR)/main.elf
	$(VOS) $(VFLAGSRUN) '$<'

run-img: $(OUTDIR)/main.img
	$(VOS) $(VFLAGSISO) '$<'

