#ifndef STRING_H
#define STRING_H

#include "common.h"

u8int strlen(const char* str);
void reverse(char str[]);
void strcpy(const char source[], char dest[]);
char* itoa(int num);
char* itoaEx(int num, char buf[], int base);
char* ltoa(long num);
char* ltoaEx(long num, char buf[], int base);
char* strcat(char* destination, const char* source);

#endif
