#include "monitor.h" // for io monitor communication
#include "input.h" // for input (cin)
#include "string.h"

void init() {
	clear(COLOR_MAGENTA); // fill screen with MAGENTA
	setForegroundColor(COLOR_BLACK); // set fg def color to black
	setBackgroundColor(COLOR_MAGENTA); // set bg def color to magenta
	print("This is simple first version of "); // print text without new line
	println("miniOS"); // print text and deploy new line
	println("Keep calm and wait for the new versions");
	println("miniOS v.0.1.0.5");
	print("Testing user input: ");
	char buff[2048];
	nextline(buff,2048,true);
//	char buff[1] = {nextchar()};
	println(buff);
	println("Done"); // just to make sure nextline() is working properly
}

int __attribute__((noreturn)) main() {
	init(); // Now we call loading part of code. It will load basic interface.
    while (1); // Prevent kernel from exit using infinite loop
}
