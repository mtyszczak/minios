// common.h -- Defines typedefs and some global functions.
// From JamesM's kernel development tutorials.

#ifndef COMMON_H
#define COMMON_H

#define false 0
#define true 1

// Some nice typedefs, to standardise sizes across platforms.
// These typedefs are written for 32-bit X86.
typedef unsigned int   u32int;
typedef          int   s32int;
typedef unsigned short u16int;
typedef          short s16int;
typedef unsigned char  u8int;
typedef          char  s8int;

// And here for 64-bit:
typedef unsigned long long u64int;
typedef 		 long long s64int;

typedef unsigned short wchar_t;
typedef          int   bool; // or #define bool int
typedef unsigned int   size_t; // i don't know what i'm doing -\(*-*)/-


// Write a byte out to the specified port.
void outb(u16int port, u8int value);

u8int inb(u16int port);

u16int inw(u16int port);

#endif
