#include "input.h"

#include "time.h"

char key_code = 0;

char nextchar(bool display) {
  char ch = 0;
//  char defCH = inb(KEYBOARD_PORT);
  while((ch = inb(KEYBOARD_PORT)) != 0){
    if(ch > 0 /*&& ch != defCH*/) {
		key_code = ch;
		char conv = codeToChar(ch);
		if (display) {
			char da[2] = {conv,0};
			print(da); // debug
		}
		return conv;
	}
}
  return ch;
}

char *nextline(char* buffer, int buf_size, bool display) {
//	char buffer[buf_size];

	int ccD = 0; // current char

//	puts(50,0,getDefFG(),getDefBG(), itoa(buf_size)); // debug

	while(1)
	{
		buffer[ccD] = nextchar(display);

		ccD++;

//		puts(50,1,getDefFG(),getDefBG(),itoa(ccD)); // debug

		if(key_code == KEY_ENTER || ccD>=buf_size-1) {
			buffer[ccD+1] = 0;
			break;
		}
		// Primitive:
		unsigned long start = getTime();
    	while(1)
        	if (start+100000000<getTime())
            	break;

	}
//	puts(50,2,getDefFG(),getDefBG(),"End"); // debug
	return buffer;
}
