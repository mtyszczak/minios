#include "time.h"

// TODO: https://gist.github.com/gburd/5469493

unsigned long getTime() {
	// For 64 bit systems
	#ifdef __x86_64__
		unsigned long lo, hi;
    	__asm__ volatile( "rdtsc" : "=a" (lo), "=d" (hi) : : );
    	return ( lo | (hi << 32) );
	#endif

	// For 32 bit systems
	#ifdef __i386__
		unsigned long lo, hi;
        __asm__ volatile( "rdtsc" : "=a" (lo), "=d" (hi) : : );
        return lo;
	#endif
}
