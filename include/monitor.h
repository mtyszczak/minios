#ifndef MONITOR_H
#define MONITOR_H

#include "common.h"
#include "string.h"

enum color { // enum with color codes
	COLOR_BLACK = 0,
	COLOR_BLUE = 1,
	COLOR_GREEN = 2,
	COLOR_CYAN = 3,
	COLOR_RED = 4,
	COLOR_MAGENTA = 5,
	COLOR_BROWN = 6,
	COLOR_LIGHT_GREY = 7,
	COLOR_DARK_GREY = 8,
	COLOR_LIGHT_BLUE = 9,
	COLOR_LIGHT_GREEN = 10,
	COLOR_LIGHT_CYAN = 11,
	COLOR_LIGHT_RED = 12,
	COLOR_LIGHT_MAGENTA = 13,
	COLOR_LIGHT_BROWN = 14,
	COLOR_WHITE = 15,
};
enum size { // size of the console window
    COLS = 80,
    ROWS = 25
};

void putc(u8int x, u8int y, enum color fg, enum color bg, char c);
void clear(enum color bg);
void puts(u8int x, u8int y, enum color fg, enum color bg, const char *s);
void println(char* s);
void print(char* s);
void setForegroundColor(enum color fg);
void setBackgroundColor(enum color bg);
char getc(u8int x, u8int y);
void check_scroll(void);
void move_cursor(u8int x, u8int y);
void update_cursor(void);
enum color getDefFG();
enum color getDefBG();

#endif // MONITOR_H
