#include "string.h"

// TODO: swap implementation for template<T>
void swap(int t1, int t2) {
    int tmp = t1;
    t1=t2;
    t2=tmp;
}

// Function to implement strcat() function in C
char* strcat(char* destination, const char* source) {
	// make ptr point to the end of destination string
	char* ptr = destination + strlen(destination);

	// Appends characters of source to the destination string
	while (*source != '\0')
		*ptr++ = *source++;

	// null terminate destination string
	*ptr = '\0';

	// destination is returned by standard strcat()
	return destination;
}

u8int strlen(const char *str) {
	u8int ret = 0;
	while ( str[ret] != 0 )
		ret++;
	return ret;
}

// TODO: reverse char*
/* A utility function to reverse a string  */
void reverse(char str[])
{
    int length = strlen(str);
    int start = 0;
    int end = length -1;
    while (start < end)
    {
        swap(*(str+start), *(str+end));
        start++;
        end--;
    }
}

void strcpy(const char source[], char dest[])
{
    int i = 0;
    while (1)
    {
        dest[i] = source[i];

        if (dest[i] == '\0')
        {
            break;
        }

        i++;
    }
}

char* itoa(int num) {
  char buf[100]; // buf -> 100
  return itoaEx(num, buf, 10);
}

char* ltoa(long num) {
	char buf[100];
	return ltoaEx(num,buf,10);
}

char* ltoaEx(long num, char str[], int base)
{
	int i = 0;
    bool isNegative = false;

    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
	if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = true;
        num = -num;
    }

    // Process individual digits
    while (num != 0)
    {
        long rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        num = num/base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator

    char* temp = "";
    strcpy(str, temp);
    // Reverse the string
    reverse(temp);
    return temp;

}

// Implementation of extended itoa()
char* itoaEx(int num, char str[], int base)
{
    int i = 0;
    bool isNegative = false;

    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    // In standard itoa(), negative numbers are handled only with
    // base 10. Otherwise numbers are considered unsigned.
    if (num < 0 && base == 10)
    {
        isNegative = true;
        num = -num;
    }

    // Process individual digits
    while (num != 0)
    {
        int rem = num % base;
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0';
        num = num/base;
    }

    // If number is negative, append '-'
    if (isNegative)
        str[i++] = '-';

    str[i] = '\0'; // Append string terminator

    char* temp = "";
    strcpy(str, temp);
    // Reverse the string
    reverse(temp);
    return temp;
}

